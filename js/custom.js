//preloader
jQuery(window).load(function() {
    jQuery('#preloader').fadeOut('slow', function() {
        jQuery(this).remove();
    });
});

jQuery(document).ready(function($) {
    // add class .table for all tables inside .main_text and  wrap table for responsive
    $('.main_text').find('table').addClass('table');
    $('.main_text table ').wrap('<div class="table-responsive"></div>');

    $(".head_search_icon").click(function() {
        $(".head_seach").toggleClass(" head_seach_open ");
    });

    //HP project carousel
    var slider = $(".project_carousel").lightSlider({
        item: 1,
        loop: false,
        mode: 'fade',
        speed: 600,
        controls: false,
        pager: true,
        slideMargin: 0,
        slideMove: 1
    });
    //HP project carousel next
    $('#project_carousel_next').click(function() {
        slider.goToNextSlide();
    });
    //HP project carousel prev
    $('#project_carousel_prev').click(function() {
        slider.goToPrevSlide();
    });

    //HP service carousel
    var slider1 = $(".service_carousel").lightSlider({
        item: 3,
        loop: false,
        controls: false,
        pager: true,
        slideMargin: 30,
        slideMove: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                item: 1,
                slideMargin: 20
            }
        }, {
            breakpoint: 672,
            settings: {
                item: 1
            }
        }, {
            breakpoint: 481,
            settings: {
                item: 1
            }
        }]
    });
    //HP service Slider next
    $('#service_carousel_next').click(function() {
        slider1.goToNextSlide();
    });
    //HP service Slider prev
    $('#service_carousel_prev').click(function() {
        slider1.goToPrevSlide();
    });

    //HP project2 carousel
    var slider2 = $(".project2_carouse").lightSlider({
        item: 2,
        loop: false,
        controls: false,
        pager: true,
        slideMargin: 30,
        slideMove: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                item: 1,
                slideMargin: 20
            }
        }, {
            breakpoint: 672,
            settings: {
                item: 1
            }
        }, {
            breakpoint: 481,
            settings: {
                item: 1
            }
        }]
    });
    //HP project2 Slider next
    $('#project2_carousel_next').click(function() {
        slider2.goToNextSlide();
    });
    //HP project2 Slider prev
    $('#project2_carousel_prev').click(function() {
        slider2.goToPrevSlide();
    });

    //HP replay carousel
    var slider3 = $(".hp_replay_carousel").lightSlider({
        item: 1,
        loop: false,
        mode: 'fade',
        speed: 600,
        controls: false,
        pager: true,
        slideMargin: 30,
        slideMove: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                item: 1,
                slideMargin: 20
            }
        }, {
            breakpoint: 672,
            settings: {
                item: 1
            }
        }, {
            breakpoint: 481,
            settings: {
                item: 1
            }
        }]
    });
    //HP replay Slider next
    $('#replay_carousel_next').click(function() {
        slider3.goToNextSlide();
    });
    //HP replay Slider prev
    $('#replay_carousel_prev').click(function() {
        slider3.goToPrevSlide();
    });

    // light Gallery
    $(".replay_gallery").lightGallery({
        thumbnail: true,
        animateThumb: true
    });

    //WORK single carousel
    var slider4 = $(".work_single_carousel").lightSlider({
        item: 1,
        loop: true,
        controls: false,
        pager: true,
        slideMargin: 30,
        slideMove: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                item: 1,
                slideMargin: 20
            }
        }, {
            breakpoint: 672,
            settings: {
                item: 1
            }
        }, {
            breakpoint: 481,
            settings: {
                item: 1
            }
        }]
    });
    //WORK single carousel  next
    $('#work_carousel_next').click(function() {
        slider4.goToNextSlide();
    });
    //WORK single carousel  prev
    $('#work_carousel_prev').click(function() {
        slider4.goToPrevSlide();
    });

    $(".about_carousel").lightSlider({
        item: 1,
        loop: true,
        controls: false,
        pager: true,
        slideMargin: 30,
        slideMove: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                item: 1,
                slideMargin: 20
            }
        }, {
            breakpoint: 672,
            settings: {
                item: 1
            }
        }, {
            breakpoint: 481,
            settings: {
                item: 1
            }
        }]
    });

    //HP example carousel
    var slider5 = $(".example_carousel").lightSlider({
        item: 3,
        loop: false,
        controls: false,
        pager: true,
        slideMargin: 30,
        slideMove: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                item: 2,
                slideMargin: 20
            }
        }, {
            breakpoint: 672,
            settings: {
                item: 1
            }
        }, {
            breakpoint: 481,
            settings: {
                item: 1
            }
        }]
    });
    //HP example Slider next
    $('#example_carousel_next').click(function() {
        slider5.goToNextSlide();
    });
    //HP example Slider prev
    $('#example_carousel_prev').click(function() {
        slider5.goToPrevSlide();
    });

    //HP service_example carousel
    var slider5 = $(".service_example_carousel").lightSlider({
        item: 2,
        loop: false,
        controls: false,
        pager: true,
        slideMargin: 30,
        slideMove: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                item: 2,
                slideMargin: 20
            }
        }, {
            breakpoint: 672,
            settings: {
                item: 1
            }
        }, {
            breakpoint: 481,
            settings: {
                item: 1
            }
        }]
    });
    //HP example Slider next
    $('#service_example_carousel_next').click(function() {
        slider5.goToNextSlide();
    });
    //HP example Slider prev
    $('#service_example_carousel_prev').click(function() {
        slider5.goToPrevSlide();
    });


    //Lazu YT
    // 

    //ancher menu
    // jQuery('.anchor_menu').onePageNav({
    //     scrollSpeed: 750,
    //     scrollThreshold: 0.5,
    //     scrollOffset: 45
    // });

    // scroll to form section
    $(".brea_ttl_area .white_btn").click(function() {
        $('html, body').animate({
            scrollTop: $(".block_has_form").offset().top - 0
        }, 1000);
        return false;
    });
});
